﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using mcv_app1.Models;

namespace mcv_app1.Controllers
{
    public class MetrostationsController : Controller
    {
        private OrderDB db = new OrderDB();

        //
        // GET: /Metrostations/

        public ActionResult Index()
        {
            return View(db.Metrostations.ToList());
        }

        //
        // GET: /Metrostations/Details/5

        public ActionResult Details(int id = 0)
        {
            Metrostation metrostation = db.Metrostations.Find(id);
            if (metrostation == null)
            {
                return HttpNotFound();
            }
            return View(metrostation);
        }

        //
        // GET: /Metrostations/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Metrostations/Create

        [HttpPost]
        public ActionResult Create(Metrostation metrostation)
        {
            if (ModelState.IsValid)
            {
                db.Metrostations.Add(metrostation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(metrostation);
        }

        //
        // GET: /Metrostations/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Metrostation metrostation = db.Metrostations.Find(id);
            if (metrostation == null)
            {
                return HttpNotFound();
            }
            return View(metrostation);
        }

        //
        // POST: /Metrostations/Edit/5

        [HttpPost]
        public ActionResult Edit(Metrostation metrostation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(metrostation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(metrostation);
        }

        //
        // GET: /Metrostations/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Metrostation metrostation = db.Metrostations.Find(id);
            if (metrostation == null)
            {
                return HttpNotFound();
            }
            return View(metrostation);
        }

        //
        // POST: /Metrostations/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Metrostation metrostation = db.Metrostations.Find(id);
            db.Metrostations.Remove(metrostation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}