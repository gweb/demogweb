﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using mcv_app1.Models;

namespace mcv_app1.Controllers
{
    public class DeliverymanController : Controller
    {
        private OrderDB db = new OrderDB();

        //
        // GET: /Deliveryman/

        public ActionResult Index()
        {
            return View(db.Deliverymen.ToList());
        }

        //
        // GET: /Deliveryman/Details/5

        public ActionResult Details(int id = 0)
        {
            Deliveryman deliveryman = db.Deliverymen.Find(id);
            if (deliveryman == null)
            {
                return HttpNotFound();
            }
            return View(deliveryman);
        }

        //
        // GET: /Deliveryman/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Deliveryman/Create

        [HttpPost]
        public ActionResult Create(Deliveryman deliveryman)
        {
            if (ModelState.IsValid)
            {
                db.Deliverymen.Add(deliveryman);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(deliveryman);
        }

        //
        // GET: /Deliveryman/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Deliveryman deliveryman = db.Deliverymen.Find(id);
            if (deliveryman == null)
            {
                return HttpNotFound();
            }
            return View(deliveryman);
        }

        //
        // POST: /Deliveryman/Edit/5

        [HttpPost]
        public ActionResult Edit(Deliveryman deliveryman)
        {
            if (ModelState.IsValid)
            {
                db.Entry(deliveryman).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(deliveryman);
        }

        //
        // GET: /Deliveryman/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Deliveryman deliveryman = db.Deliverymen.Find(id);
            if (deliveryman == null)
            {
                return HttpNotFound();
            }
            return View(deliveryman);
        }

        //
        // POST: /Deliveryman/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Deliveryman deliveryman = db.Deliverymen.Find(id);
            db.Deliverymen.Remove(deliveryman);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}