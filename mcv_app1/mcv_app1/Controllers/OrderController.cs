﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using mcv_app1.Models;

namespace mcv_app1.Controllers
{
    public class OrderController : Controller
    {
        private OrderDB db = new OrderDB();

        //
        // GET: /Order/

        public ActionResult Index()
        {
            var orders = db.Orders.Include(o => o.Deliverymen).Include(o => o.Metrostations);
            return View(orders.ToList());
        }

        //
        // GET: /Order/Details/5

        public ActionResult Details(int id = 0)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        //
        // GET: /Order/Create

        public ActionResult Create()
        {
            ViewBag.DeliverymanId = new SelectList(db.Deliverymen, "DeliverymanId", "FIO");
            ViewBag.MetrostationId = new SelectList(db.Metrostations, "MetrostationId", "Name");
            return View();
        }

        //
        // POST: /Order/Create

        [HttpPost]
        public ActionResult Create(Order order)
        {
            if (ModelState.IsValid)
            {
                db.Orders.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DeliverymanId = new SelectList(db.Deliverymen, "DeliverymanId", "FIO", order.DeliverymanId);
            ViewBag.MetrostationId = new SelectList(db.Metrostations, "MetrostationId", "Name", order.MetrostationId);
            return View(order);
        }

        //
        // GET: /Order/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.DeliverymanId = new SelectList(db.Deliverymen, "DeliverymanId", "FIO", order.DeliverymanId);
            ViewBag.MetrostationId = new SelectList(db.Metrostations, "MetrostationId", "Name", order.MetrostationId);
            return View(order);
        }

        //
        // POST: /Order/Edit/5

        [HttpPost]
        public ActionResult Edit(Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DeliverymanId = new SelectList(db.Deliverymen, "DeliverymanId", "FIO", order.DeliverymanId);
            ViewBag.MetrostationId = new SelectList(db.Metrostations, "MetrostationId", "Name", order.MetrostationId);
            return View(order);
        }

        //
        // GET: /Order/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        //
        // POST: /Order/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}