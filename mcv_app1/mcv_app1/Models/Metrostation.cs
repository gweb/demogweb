﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mcv_app1.Models
{
    public class Metrostation
    {
        public int MetrostationId { get; set; }
        public string Name { get; set; }
        public Deliveryman Deliveryman { get; set; }
    }
}