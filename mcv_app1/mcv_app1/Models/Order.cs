﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mcv_app1.Models
{
    public class Order
    {
        public int OrderId { get; set; } 
        public int DeliverymanId { get; set; }
        public int MetrostationId { get; set; }
        public string Type { get; set; }
        public float Price { get; set; }
        public Deliveryman Deliverymen { get; set; }
        public Metrostation Metrostations { get; set; }
    }
}