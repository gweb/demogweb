﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mcv_app1.Models
{
    public class Deliveryman
    {
        public int DeliverymanId { get; set; }
        public string FIO { get; set; }
        public List<Metrostation> Metrostations { get; set; }
        //public Metrostation Metrostation { get; set; }
    }
}